import React, { useState, useEffect } from "react";
import { CONST } from "../../utils/Constants";
import { PackOfLeafs } from "./Leafs.styles";
import { calculateObjectPosition, generateMovingObject } from "../../utils/Functions";

let leafLineCounter = 0;
let leafInterval;
let waterTop;
let waterHeight;
let drowned = false;

export default function Leafs() {
  useEffect(() => {
    const leafs = document.querySelectorAll("." + CONST.LEAF_CLASSNAME);
    setWaterSize();

    leafInterval = setInterval(() => {
      processNewLeafPosition(leafs);
    }, CONST.LEAF_SPEED_INTERVAL);
  }, []);

  const setWaterSize = () => {
    const water = document.getElementById(CONST.TOP_DANGER_ZONE_ID);
    waterTop = Math.floor(water.getBoundingClientRect().top);
    waterHeight = Math.floor(water.getBoundingClientRect().height);
  };

  const processNewLeafPosition = (leafs) => {
    drowned = true;

    leafs.forEach(leaf => {
      calculateObjectPosition(leaf);
      checkTouching(leaf);
    });

    if (drowned) {
      alert("END GAME!!!");
      location.reload();
      clearInterval(leafInterval);
    }
  };

  const checkTouching = (leaf) => {
    let plTop = parseInt(localStorage.getItem(CONST.PL_TOP_POS));
    let plLeft = parseInt(localStorage.getItem(CONST.PL_LEFT_POS));

    if (plTop < waterTop + waterHeight && plTop + CONST.PLAYER_SIZE > waterTop) {
      let leafPosition = leaf.getBoundingClientRect();
      let leafTop = parseInt(leafPosition.top);
      let leafLeft = parseInt(leafPosition.left);
      let leafWidth = parseInt(leafPosition.width);

      if (plTop < leafTop + CONST.LEAF_HEIGHT && plTop + CONST.PLAYER_SIZE > leafTop) {
        if (plLeft + CONST.PLAYER_SIZE > leafLeft && plLeft < leafLeft + leafWidth) {
          drowned = false;
        }
      }
    } else {
      drowned = false;
    }
  };

  const generateLinesOfLeafs = () => {
    return [
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * leafLineCounter,
        40,
        6,
        5,
        CONST.LEAF_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++leafLineCounter,
        60,
        6,
        2,
        CONST.LEAF_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++leafLineCounter,
        60,
        6,
        5,
        CONST.LEAF_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++leafLineCounter,
        80,
        6,
        1,
        CONST.LEAF_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++leafLineCounter,
        80,
        6,
        3,
        CONST.LEAF_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++leafLineCounter,
        100,
        6,
        2,
        CONST.LEAF_CLASSNAME
      )
    ];
  };

  return (
    <>
      <PackOfLeafs>
        {generateLinesOfLeafs()}
      </PackOfLeafs>
    </>
  );
}
