import styled from "styled-components";
import { CONST } from "../../utils/Constants";

export const PackOfLeafs = styled.div`
  position:absolute;
  width:100%;
  height:211px;
  top:26px;
`;

export const Leaf = styled.div`
  width:${({width}) => width}px;
  height:${CONST.LEAF_HEIGHT}px;
  top:${({top}) => top}px;
  border-radius:20%;
  position:absolute;
  background:green;
`;
