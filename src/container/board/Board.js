import React, { useState } from "react";
import { Playground } from "./Board.styles";
import Zones from "../zones/Zones";
import Enemies from "../enemies/Enemies";
import Player from "../player/Player";
import Leafs from "../leafs/Leafs";

export default class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Playground>
        <Zones />
        <Leafs />
        <Enemies />
        <Player />
      </Playground>
    );
  }
}
