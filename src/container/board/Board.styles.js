import styled from "styled-components";
import {CONST} from "../../utils/Constants";

export const Playground = styled.div`
  width:${CONST.PLAYGROUND_WIDTH}px;
  height:auto;
  display:block;
  margin-left:auto;
  margin-right:auto;
  border:2px solid black;
  position:relative;
  overflow:hidden;
`;
