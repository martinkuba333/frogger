import React, { useState, useEffect, useContext } from "react";
import { PlayerIcon } from "./Player.styles";
import { CONST } from "../../utils/Constants";

export default function Player(props) {
  const [plTop, setPlTop] = useState(CONST.ALL_ZONES_HEIGHT - CONST.PLAYER_SIZE - 3);
  const [plLeft, setPlLeft] = useState(CONST.PLAYGROUND_WIDTH / 2);
  const [stop, setStop] = useState(false);

  useEffect(() => {
    if (!stop) {
      window.addEventListener("keypress", movePlayer);
      return () => window.removeEventListener("keypress", movePlayer);
    }
  }, [stop]);

  const movePlayer = (e) => {
    const plMoveVertical = CONST.ENEMY_INCREASE_GAP;
    const plMoveHorizontal = CONST.PLAYER_SIZE;

    if (e.key === CONST.UP_MOVE) {
      setPlTop(state => state - plMoveVertical > 0 ? state - plMoveVertical : state);
    }
    if (e.key === CONST.DOWN_MOVE) {
      setPlTop(state => state + plMoveVertical < CONST.ALL_ZONES_HEIGHT ? state + plMoveVertical : state);
    }
    if (e.key === CONST.LEFT_MOVE) {
      setPlLeft(state => state - plMoveHorizontal > 0 ? state - plMoveHorizontal : state);
    }
    if (e.key === CONST.RIGHT_MOVE) {
      setPlLeft(state => state + plMoveHorizontal < CONST.PLAYGROUND_WIDTH - CONST.PLAYER_SIZE ? state + plMoveHorizontal : state);
    }
  };

  const processToNextFrog = () => {
    //todo? three frogs
    checkIfWin();
  };

  const checkIfWin = () => {
    if (!stop && plTop < CONST.PLAYER_SIZE) {
      setStop(true);
      showAlert(CONST.WIN);
      location.reload();
    }
  };

  const showAlert = (status) => {
    const randomTimeoutNumber = 100;
    setTimeout(() => {
      alert(status);
    }, randomTimeoutNumber);
  };

  const setPositionToStorage = () => {
    processToNextFrog();

    let player = document.getElementById(CONST.PLAYER_ID);
    if (player) {
      setTimeout(() => {
        let playerPosition = player.getBoundingClientRect();

        localStorage.setItem(CONST.PL_TOP_POS, String(playerPosition.top));
        localStorage.setItem(CONST.PL_LEFT_POS, String(playerPosition.left));
      }, 1);
    } else {
      const randomBigNumber = 1000;
      localStorage.setItem(CONST.PL_TOP_POS, String(randomBigNumber));
      localStorage.setItem(CONST.PL_LEFT_POS, String(randomBigNumber));
    }
  };

  const drawPlayer = () => {
    setPositionToStorage();

    return (
      <PlayerIcon
        id={CONST.PLAYER_ID}
        style={{
          left: plLeft + "px",
          top: plTop + "px"
        }}
      />);
  };

  return (
    <>
      {drawPlayer()}
    </>
  );
}
