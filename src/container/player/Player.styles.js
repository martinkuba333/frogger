import styled from "styled-components";
import {CONST} from "../../utils/Constants";

export const PlayerIcon = styled.div`
  width:${CONST.PLAYER_SIZE}px;
  height:${CONST.PLAYER_SIZE}px;
  background: red;
  position:absolute;
`;
