import React, { useState, useEffect, useContext } from "react";
import { Trucks } from "./Enemies.styles";
import { CONST } from "../../utils/Constants";
import { calculateObjectPosition, generateMovingObject } from "../../utils/Functions";

let truckLineCounter = 0;
let truckInterval;
let streetTop;
let streetHeight;
let crashed = false;

export default function Enemies() {
  useEffect(() => {
    const trucks = document.querySelectorAll(".truck");
    setStreetSize();

    truckInterval = setInterval(() => {
      processNewTruckPosition(trucks);
    }, CONST.TRUCK_SPEED_INTERVAL);
  }, []);

  const setStreetSize = () => {
    const street = document.getElementById(CONST.BOTTOM_DANGER_ZONE_ID);
    streetTop = street.getBoundingClientRect().top;
    streetHeight = street.getBoundingClientRect().height;
  };

  const processNewTruckPosition = (trucks) => {
    trucks.forEach(truck => {
      calculateObjectPosition(truck);
      checkCrash(truck);
    });

    if (crashed) {
      clearInterval(truckInterval);
      alert("HIT!!! END GAME!!! YOU DEAD!!!");
      location.reload();
    }
  };

  const checkCrash = (truck) => {
    let plTop = parseInt(localStorage.getItem(CONST.PL_TOP_POS));
    let plLeft = parseInt(localStorage.getItem(CONST.PL_LEFT_POS));

    if (plTop < streetTop + streetHeight && plTop + CONST.PLAYER_SIZE > streetTop) {
      let truckPosition = truck.getBoundingClientRect();
      let truckTop = parseInt(truckPosition.top);
      let truckLeft = parseInt(truckPosition.left);
      let truckWidth = parseInt(truckPosition.width);

      if (plTop < truckTop + CONST.TRUCK_HEIGHT && plTop + CONST.PLAYER_SIZE > truckTop) {
        if (plLeft + CONST.PLAYER_SIZE > truckLeft && plLeft < truckLeft + truckWidth) {
          crashed = true;
        }
      }
    }
  };

  const generateLinesOfTrucks = () => {
    return [
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * truckLineCounter,
        20,
        6,
        7,
        CONST.TRUCK_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++truckLineCounter,
        60,
        4,
        -2,
        CONST.TRUCK_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++truckLineCounter,
        80,
        4,
        1,
        CONST.TRUCK_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++truckLineCounter,
        50,
        4,
        3,
        CONST.TRUCK_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++truckLineCounter,
        70,
        4,
        -3,
        CONST.TRUCK_CLASSNAME
      ),
      generateMovingObject(
        CONST.ENEMY_FIRST_LINE + CONST.ENEMY_INCREASE_GAP * ++truckLineCounter,
        90,
        4,
        1,
        CONST.TRUCK_CLASSNAME
      )
    ];
  };

  return (
    <>
      <Trucks>
        {generateLinesOfTrucks()}
      </Trucks>
    </>
  );
}
