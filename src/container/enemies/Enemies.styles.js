import styled from "styled-components";
import { CONST } from "../../utils/Constants";

export const Trucks = styled.div`
  position: absolute;
  top: 265px;
  width: 100%;
  height: 205px;
`;

export const Truck = styled.div`
  height: ${CONST.TRUCK_HEIGHT}px;
  background: black;
  display: block;
  position: absolute;
  top: ${({ top }) => top}px;
  width: ${({ width }) => width}px;
`;
