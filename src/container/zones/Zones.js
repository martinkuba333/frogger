import React, { useState } from "react";
import {CONST} from "../../utils/Constants";
import {
  BottomSafeZone,
  BottomDangerZone,
  TopDangerZone,
  MiddleSafeZone,
  TopSafeZone,
  AllZones
} from "./Zones.styles";

function Zones() {
  return (
    <AllZones>
      <TopSafeZone />
      <TopDangerZone id={CONST.TOP_DANGER_ZONE_ID} />
      <MiddleSafeZone />
      <BottomDangerZone id={CONST.BOTTOM_DANGER_ZONE_ID} />
      <BottomSafeZone />
    </AllZones>
  );
}

export default Zones;
