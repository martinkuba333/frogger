import styled from "styled-components";
import {CONST} from "../../utils/Constants";

const safeZoneFlexGrow = 1;
const dangerZoneFlexGrow = 8;

export const AllZones = styled.div`
  display:flex;
  flex-direction:column;
  box-sizing: border-box;
  min-height:${CONST.ALL_ZONES_HEIGHT}px;
`;

export const TopSafeZone = styled.div`
  background: lightgreen;
  flex-grow:${safeZoneFlexGrow};
`;

export const TopDangerZone = styled.div`
  background: aliceblue;
  flex-grow:${dangerZoneFlexGrow};
`;

export const MiddleSafeZone = styled.div`
  background: lightgreen;
  flex-grow:${safeZoneFlexGrow};
`;

export const BottomDangerZone = styled.div`
  background: grey;
  flex-grow:${dangerZoneFlexGrow};  
`;

export const BottomSafeZone = styled.div`
  background: lightgreen;
  flex-grow:${safeZoneFlexGrow};
`;
