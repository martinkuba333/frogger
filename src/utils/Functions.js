import React from "react";
import { Truck } from "../container/enemies/Enemies.styles";
import { Leaf } from "../container/leafs/Leafs.styles";
import { CONST } from "./Constants";

export const generateMovingObject = (top, width, sum, speed, className) => {
  const movingObject = [];
  const randomMaxNumber = 90;

  for (let i = 0; i < sum; i++) {
    let randomNum = Math.floor(Math.random() * randomMaxNumber);
    let leftPosition = i * (width + randomMaxNumber) - (randomNum);
    let tempObject = fillObject(top, width, sum, speed, className, leftPosition, i);

    movingObject.push(tempObject);
  }
  return movingObject;
};

const fillObject = (top, width, sum, speed, className, leftPosition, i) => {
  if (className === CONST.TRUCK_CLASSNAME) {
    return <Truck
      key={i}
      className={className}
      top={top}
      width={width}
      data-speed={speed}
      style={{ left: leftPosition + "px" }}
    />;

  } else {
    return <Leaf
      key={i}
      className={className}
      top={top}
      width={width}
      data-speed={speed}
      style={{ left: leftPosition + "px" }}
    />;
  }
};

export const calculateObjectPosition = (object) => {
  const numProcessStartEnd = 100;
  let leftPos = parseInt(window.getComputedStyle(object).left);
  let speed = object.getAttribute("data-speed");
  if (speed > 0 && leftPos < -numProcessStartEnd) {
    leftPos = CONST.PLAYGROUND_WIDTH + numProcessStartEnd;
  }
  if (speed < 0 && leftPos > CONST.PLAYGROUND_WIDTH + numProcessStartEnd) {
    leftPos = -numProcessStartEnd;
  }
  let newObjectLeftPos = leftPos - speed;
  object.style.left = newObjectLeftPos + "px";
};
