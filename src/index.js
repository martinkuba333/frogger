import React from "react";
import ReactDOM from "react-dom";
import Main from "./container/main/Main";

ReactDOM.render(
  <>
    <Main />
  </>,
  document.getElementById("board")
);
